# scaamanho/grafana
Grafana with custom panel plugins and Swarm Cluster monitoring panels based on Prometheus.

build image
`docker build -t scaamanho/grafana:docker.swarm .`



## Usage
`docker run -d -p 3000:3000 scaamanho/grafana:docker.swarm`

**Prometheus mus be accesible on same network as graphana and located at url <http://prometheus:9090>**, else change **`datasource-prometheus.yml`** file.

## Pre-installed visualization panels
  * grafana-worldmap-panel
  * grafana-piechart-panel
  * petrslavotinek-carpetplot-panel
  * neocat-cal-heatmap-panel
  * briangann-gauge-panel
  * jdbranham-diagram-panel
  * citilogics-geoloop-panel
  * mtanda-histogram-panel
  * mtanda-heatmap-epoch-panel
  * natel-plotly-panel 
  * bessler-pictureit-panel

## Preinstaled dashboards
  * docker swarm monitor
  * docker swarm server
  * docker swarm container
  * prometheus