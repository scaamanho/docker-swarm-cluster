# scaamanho/grafana
Grafana with custom panel plugins.

build image
`docker build -t scaamanho/grafana:3.6.5 .`

This is an extension of the great monitoring work on Swarm Clusters of github.com/stefanprodan

## Usage
`docker run -d -p 3000:3000 scaamanho/grafana:docker.swarm`

## Pre-installed visualization panels
  * grafana-worldmap-panel
  * grafana-piechart-panel
  * petrslavotinek-carpetplot-panel
  * neocat-cal-heatmap-panel
  * briangann-gauge-panel
  * jdbranham-diagram-panel
  * citilogics-geoloop-panel
  * mtanda-histogram-panel
  * mtanda-heatmap-epoch-panel
  * natel-plotly-panel 
  * bessler-pictureit-panel

