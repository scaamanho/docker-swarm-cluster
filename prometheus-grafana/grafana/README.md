# scaamanho/grafana

Customized grafana vitaminated.


# scaamanho/grafana:3.6.5

### Build 

`docker build -t scaamanho/grafana:3.6.5 ./3.6.5`

### Description

Grafana with extra panels instaled by default.

* mtanda-heatmap-epoch-panel
* grafana-worldmap-panel
* grafana-piechart-panel
* petrslavotinek-carpetplot-panel
* neocat-cal-heatmap-panel
* briangann-gauge-panel
* jdbranham-diagram-panel
* citilogics-geoloop-panel
* natel-plotly-panel 
* bessler-pictureit-panel
* grafana-clock-panel
* novalabs-annotations-panel
* digrich-bubblechart-panel
* briangann-datatable-panel
* natel-discrete-panel
* savantly-heatmap-panel
* snuids-radar-panel
* blackmirror1-statusbygroup-panel
* vonage-status-panel
* zuburqan-parity-report-panel


# scaamanho/grafana:docker.swarm

### Build 

`docker build -t scaamanho/grafana:docker.swarm ./docker.swarm`

customized scaamanho/grafana:3.6.5 ready for docker swarm deployment by default

### Description

**Prometheus mus be accesible on same network as graphana and located at url <http://prometheus:9090>**, else change **`datasource-prometheus.yml`** file.

Grafana configured with prometheus integration and docker swarm cluster.

* All features from scaamanho/graphana:3.6.5
* Custom Dashboards instaled
    * docker swarm monitor
    * docker swarm server stats
    * docker swarm container stats
    * prometheus

### Sample deployment file

```yml
version: '3.5'

networks:
  frontend: # Rename traefik-network as frontend
    external:
      name: traefik-network
  backend: 

      
volumes:
  grafana-data:

services:
  grafana:
    image: scaamanho/grafana:docker.swarm
    environment:
      #-GF_INSTALL_PLUGINS: "grafana-clock-panel,grafana-simple-json-datasource"
      - GF_SECURITY_ADMIN_USER=${ADMIN_USER:-admin}
      - GF_SECURITY_ADMIN_PASSWORD=${ADMIN_PASSWORD:-admin}
      - GF_USERS_ALLOW_SIGN_UP=false
      #- GF_PATHS_PROVISIONING=/etc/grafana/provisioning/
      #- GF_SERVER_ROOT_URL=${GF_SERVER_ROOT_URL:-localhost}
      #- GF_SMTP_ENABLED=${GF_SMTP_ENABLED:-false}
      #- GF_SMTP_FROM_ADDRESS=${GF_SMTP_FROM_ADDRESS:-grafana@test.com}
      #- GF_SMTP_FROM_NAME=${GF_SMTP_FROM_NAME:-Grafana}
      #- GF_SMTP_HOST=${GF_SMTP_HOST:-smtp:25}
      #- GF_SMTP_USER=${GF_SMTP_USER}
      #- GF_SMTP_PASSWORD=${GF_SMTP_PASSWORD}
    volumes: 
      - grafana-data:/var/lib/grafana
    ports:
      - "3000:3000"
    deploy:
    #  labels:
    #    traefik.port: "3000"
    #    traefik.frontend.rule: "Host:grafana.${CLUSTER_DOMAIN}"
    #    traefik.docker.network: "traefik-network" 
    #  placement:
    #    constraints:
    #      - node.role == managerresources:
      limits:
        memory: 128M
      reservations:
        memory: 64M
        
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:3000"]
      interval: 5s
      timeout: 1s
      retries: 5

    networks:
      - frontend
      - backend
```