https://blog.ruanbekker.com/blog/2018/04/29/running-a-3-node-elasticsearch-cluster-with-docker-compose-on-your-laptop-for-testing/

Cluster de elastic search

```
version: '3'
services:
  es01:
    container_name: es01
    image: docker.elastic.co/elasticsearch/elasticsearch:7.1.1
    environment:
      - node.name=es01
      - discovery.seed_hosts=es02
      - cluster.initial_master_nodes=es01,es02
      - bootstrap.memory_lock=false
      - "ES_JAVA_OPTS=-Xms4096m -Xmx4096m"
    volumes:
      - esdata01:/usr/share/elasticsearch/data
    ports:
      - 9200:9200
    deploy:
      mode: replicated
      replicas: 2

  es02:
    container_name: es02
    image: docker.elastic.co/elasticsearch/elasticsearch:7.1.1
    environment:
      - node.name=es02
      - discovery.seed_hosts=es01
      - cluster.initial_master_nodes=es01,es02
      - bootstrap.memory_lock=false
      - "ES_JAVA_OPTS=-Xms4096m -Xmx4096m"
    volumes:
      - esdata02:/usr/share/elasticsearch/data
    deploy:
      mode: replicated
      replicas: 2

volumes:
  esdata01:
    driver: local
  esdata02:
    driver: local
```

## Enable logging on Docker services

```
services:
  service-name:
    image: image-name:x.y.z
    logging:
      driver: fluentd
      options:
          fluentd-address: fluentd-service-name:24224
          fluentd-async-connect: 'true'
          fluentd-retry-wait: '1s'
          fluentd-max-retries: '30'
          tag: fluentd.docker.key   
```

* [ ] deploy.yml
* [ ] fichero configuracion fluentd
* [ ] RED de elastics search y de efk
* [ ] implementar elk
* 



```
version: '2.2'
services:
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.2.4
    container_name: elasticsearch
    environment:
      - cluster.name=docker-cluster
      - bootstrap.memory_lock=true
      - http.cors.enabled=true
      - http.cors.allow-origin=*
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - esdata1:/usr/share/elasticsearch/data
    ports:
      - 9200:9200
    networks:
      - esnet
  elasticsearch2:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.2.4
    container_name: elasticsearch2
    environment:
      - cluster.name=docker-cluster
      - bootstrap.memory_lock=true
      - http.cors.enabled=true
      - http.cors.allow-origin=*
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
      - "discovery.zen.ping.unicast.hosts=elasticsearch"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - esdata2:/usr/share/elasticsearch/data
    networks:
      - esnet
  elasticsearch3:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.2.4
    container_name: elasticsearch3
    environment:
      - cluster.name=docker-cluster
      - bootstrap.memory_lock=true
      - http.cors.enabled=true
      - http.cors.allow-origin=*
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
      - "discovery.zen.ping.unicast.hosts=elasticsearch"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - esdata3:/usr/share/elasticsearch/data
    networks:
      - esnet

  kibana:
    image: 'docker.elastic.co/kibana/kibana:6.3.2'
    container_name: kibana
    environment:
      SERVER_NAME: kibana.local
      ELASTICSEARCH_URL: http://elasticsearch:9200
    ports:
      - '5601:5601'
    networks:
      - esnet

  headPlugin:
    image: 'mobz/elasticsearch-head:5'
    container_name: head
    ports:
      - '9100:9100'
    networks:
      - esnet

volumes:
  esdata1:
    driver: local
  esdata2:
    driver: local
  esdata3:
    driver: local

networks:
  esnet:
```