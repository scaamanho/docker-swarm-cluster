https://better-coding.com/building-apache-kafka-cluster-using-docker-compose-and-virtualbox/

https://medium.com/@NegiPrateek/wtf-setting-up-kafka-cluster-using-docker-swarm-6429bdb5784b

https://linuxhint.com/docker_compose_kafka/

https://codeblog.dotsandbrackets.com/highly-available-kafka-cluster-docker/


```
version: "3"

services:
  zookeeper:
    image: confluent/zookeeper
    ports:
      - "2181:2181"
    environment:
      zk_id: "1"
    networks:
      - back-tier

  kafka:
    image: confluent/kafka
    depends_on:
      - zookeeper
    ports:
      - "9092:9092"
    environment:
      KAFKA_ZOOKEEPER_CONNECT: "zookeeper:2181"
    networks:
      - back-tier
      
networks:
  back-tier:
```

https://nathancatania.com/posts/deploying-a-multi-node-kafka-cluster-with-docker