https://medium.com/hepsiburadatech/implementing-highly-available-rabbitmq-cluster-on-docker-swarm-using-consul-based-discovery-45c4e7919634

https://github.com/olgac/haproxy-for-rabbitmq
https://github.com/olgac/rabbitmq
https://raw.githubusercontent.com/olgac/consul/master/docker-compose.yml




version: '3.5'

volumes:
  rabbit_mq_data:
  
services:
 
    rabbitmq:
        image: rabbitmq:3.7.5-management
        hostname: app-rabbitmq
        ports:
            - 5672:5672
            - 15672:15672
        volumes:
            - rabbit_mq_data:/var/lib/rabbitmq/mnesia/rabbit@app-rabbitmq:cached
        environment:
            RABBITMQ_ERLANG_COOKIE: 6085e2412b6fa88647466c6a81c0cea0
            RABBITMQ_DEFAULT_USER: rabbitmq
            RABBITMQ_DEFAULT_PASS: rabbitmq
            RABBITMQ_DEFAULT_VHOST: /

version: '3'

services:

  rabbitmq:
    image: "rabbitmq:3-management"
    hostname: "rabbit"
    ports:
      - "15672:15672"
      - "5672:5672"
    labels:
      NAME: "rabbitmq"
    volumes:
      - ./rabbitmq-isolated.conf:/etc/rabbitmq/rabbitmq.config
       #- "./rabbitmq.config:/etc/rabbitmq/rabbitmq.config:ro"
       #- "./autocluster-0.4.1.ez:/usr/lib/rabbitmq/lib/rabbitmq_server-3.5.5/plugins/autocluster-0.4.1.ez"