# docker swarm cluster

This project is aimed to build a docker swarm cluster.
With `docker-machine` or on premise.

This cluster will be provided with:

*  Traefik
* Portainer
* Grafana-Prometheus
* Dashboad
* Feeds
* Janitor

![Swarm Cluster Modules](assets/modules.svg)


## 1.- Create swarm cluster machines with docker-machine

We going to create 3 node cluster with docker-machine. 

```
$ docker-machine create  -d "virtualbox"  \
	--virtualbox-hostonly-cidr "192.168.100.1/24"  \
	--virtualbox-memory "2048" \
	--virtualbox-disk-size "30000" \
	--virtualbox-cpu-count "1" \
	manager
	
$ docker-machine create  -d "virtualbox"  \
	--virtualbox-hostonly-cidr "192.168.100.1/24"  \
	--virtualbox-memory "2048" \
	--virtualbox-disk-size "30000" \
	--virtualbox-cpu-count "1" \
	worker1

$ docker-machine create  -d "virtualbox"  \
	--virtualbox-hostonly-cidr "192.168.100.1/24"  \
	--virtualbox-memory "2048" \
	--virtualbox-disk-size "30000" \
	--virtualbox-cpu-count "1" \
Worker2	
```

The 3 machines created will have:
  * IP in range 192.168.100.100 to 192.168.100.254
  * Memory: 2G
  * Disk: 30G
  * Num CPUs:1

## 2.- Set `/etc/host` file 

We going to simulate a DNS server with the host file in the machiche where run docker machines

Add the folowwing line to your host file:

```
192.168.100.100	ds.int portainer.ds.int feeds.ds.int manager.ds.int prometheus.ds.int dashboard.ds.int grafana.ds.int
```

## 3.- Create swarm cluster

Initialize Swarm Manager
```
$ docker-machine ssh manager 
$ docker swarm init  --advertise-addr 192.168.100.100
Swarm initialized: current node (sr71ecj03elzkl9c3aaaeirh2) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-0y7xpgxrjycgcsygyrws5elskacgtbnetodisytr187v88hh4d-8z6p1bmuqpumgvwma069cm2ns 192.168.99.100:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
$ exit
```

Join Worker1 to cluster
```
$ docker-machine ssh worker1
$ docker swarm join --token SWMTKN-1-28ozr55eozabja0p792imgki8vtnua2dmhwx7k4ac3h0w7s3t6-c2nfd9akegahwfrzc8cmbbqk9 192.168.100.100:2377
$ exit
```

Join Worker2 to cluster
```
$ docker-machine ssh worker2
$ docker swarm join --token SWMTKN-1-28ozr55eozabja0p792imgki8vtnua2dmhwx7k4ac3h0w7s3t6-c2nfd9akegahwfrzc8cmbbqk9 192.168.100.100:2377
$ exit
```

Verify cluster nodes on manager
```
$ docker-machine ssh manager
$ docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
naou5ganzm6mjwo66z4e78am5 *   manager             Ready               Active              Leader              19.03.1
r05jer6czf1ksme034rcr6w1a     worker1             Ready               Active                                  19.03.1
dhtjer6czfefrme034rtsdfgs     worker2             Ready               Active                                  19.03.1

```

## 4.- Deploy `traefik-network`

Create traefik network. This network will be attached to services deployed later 

```
$ docker-machine ssh manager
$ docker network create -d overlay traefik-network
$ exit
```

## 5.- Deploy Portainer

In order to help with deployments first will deploy portainer stack. Ignore traefick labels by the moment. It will work when deploy traefick from portainer's interface.

The best way to deploy this container is from manager.

```
$ docker-machine ssh manager
$ vi portainer-agent-stack-traefik.yml
```

Paste in opened editor [portainer-agent-stack-traefik.yml](portainer/portainer-agent-stack-traefik.yml) file.
```yml
version: '3.5'

networks:
  agent_network:
    driver: overlay
    attachable: true
  traefik-network:
    external: true
    
volumes:
  portainer_data:

services:
  agent:
    image: portainer/agent
    environment:
      # REQUIRED: Should be equal to the service name prefixed by "tasks." when
      # deployed inside an overlay network
      AGENT_CLUSTER_ADDR: tasks.agent
      # AGENT_PORT: 9001
      # LOG_LEVEL: debug
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/lib/docker/volumes:/var/lib/docker/volumes
    networks:
      - agent_network
    deploy:
      mode: global
      placement:
        constraints: [node.platform.os == linux]

  portainer:
    image: portainer/portainer
    command: -H tcp://tasks.agent:9001 --tlsskipverify
    ports:
      - "9000:9000"
      #- "8000:8000"
    volumes:
      - portainer_data:/data
    networks:
      - agent_network
      - traefik-network
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints: [node.role == manager]
      labels:
        traefik.frontend.rule: Host:portainer.${DOMAIN:-ds.int}
        traefik.port: 9000
        traefik.docker.network: traefik-network
```

Deploy the stack
```
$ docker stack deploy --compose-file=portainer-agent-stack-traefik.yml portainer
```

Verify Service deployment
```
$ docker service ls
tf1kw5rznssi        portainer_agent                         global              3/3                 portainer/agent:latest
1q1tvnue3q5x        portainer_portainer                     replicated          1/1                 portainer/portainer:latest             *:9000->9000/tcp
$ exit
```

now we can acces to portainer <http://ds.int:9000> or <http://192.168.100.100:9000>


![Portainer Login](assets/portainer.login.png)

## 6.- Deploy Traefick

From Portainer Stack we deploy a new `traefik` stack with the content of [traefik-stak.yml](traefik/traefik-stak.yml) file

```yml
version: '3.5'

networks:
  traefik-network:
    external: true

services:
  traefik:
    image: traefik:1.6-alpine # The official Traefik docker image--defaultentrypoints=http 
    command: --api --docker --docker.swarmMode --docker.watch --metrics.prometheus # Enables the web UI and tells Træfik to listen to docker
    ports:
      - "80:80"     # The HTTP port
      - "3030:8080" # The Web UI (enabled by --api)
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock # So that Traefik can listen to the Docker events
    deploy:
      placement:
        constraints:
          - node.role == manager
    networks:
      - traefik-network
```

after deploy we can acces traefick dashboard http://ds.int:3030 or http://192.168.100.100:3030


## 7.- Deploy Admin Tools

Deploy on Portainer a new `admin-tools`  [admin-tools-stak.yml](admin-tools/admin-tools-stak.yml) file

### Dashboard
Show containers deployed in cluster

![Docker Swarm Dashboard](assets/dashboard.png)

http://dashboard.ds.int

### Feeds
Show feeds with avaliable traefik endpoints

![Traefik Feeds](assets/feeds.png)

http://feeds.ds.int

### Janitor
Cleanup unused docker garbage from nodes
Basically it performs `docker system prune --all` from time to time


## 8.- Deploy Prometheus-Graphana

Whit this deployment we can monitor cluster stats

Deploy on Portainer a new `prometheus-grafana` stack from [prometheus-grafana-stak.yml](prometheus-grafana/prometheus-grafana-stak.yml) file



### Grafana
![Traefik Feeds](assets/grafana.png)
http://grafana.ds.int

### Prometheus
![Traefik Feeds](assets/prometheus.png)
http://prometheus.ds.int